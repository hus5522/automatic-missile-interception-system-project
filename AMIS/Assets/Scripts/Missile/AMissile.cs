﻿using UnityEngine;
using System.Collections;

public abstract class AMissile : AProjectile
{
    public bool IsTriggered { get { return mIsTriggered; } set { mIsTriggered = value; } }

    [SerializeField]
    private GameObject mMissileBody;
    [SerializeField]
    private ParticleSystem mFlareParticle;
    [SerializeField]
    private ParticleSystem mExplosionParticle;

    private bool mIsTriggered = false;
    private bool mIsExploded = false;
    private float mTimeRemain;
    private const float END_TIME = 5.0F;

    protected sealed override bool CheckCondition()
    {
        if (!mIsExploded)
        {
            Collider[] colliders = TriggerHandler.GetColliders(ETriggerFlags_Projetile.TERRAIN);
            if (colliders[0] != null || mIsTriggered)
            {
                Collider[] detect = TriggerHandler.GetColliders(ETriggerFlags_Projetile.TARGET);
                for (int i = 0; i < detect.Length && detect[i] != null; i++)
                {
                    Area area = detect[i].GetComponent<Area>();
                    if (area != null)
                    {
                        // Area에게 정보 전달
                    }
                    else
                    {
                        AMissile missile = detect[i].GetComponent<AMissile>();
                        if (missile != null)
                        {
                            missile.IsTriggered = true;
                        }
                    }
                }

                mFlareParticle.Stop();
                mExplosionParticle.Play();
                mIsExploded = true;
                mTimeRemain = END_TIME;
                Rigidbody.velocity = Vector3.zero;
                Rigidbody.isKinematic = true;
                mMissileBody.SetActive(false);
            }
            return true;
        }
        else
        {
            if (mTimeRemain > 0.0f)
            {
                mTimeRemain -= Time.deltaTime;
                return true;
            }
            else
            {
                return false;
            }
        }

    }
    protected sealed override void Execute()
    {

    }

    protected sealed override void InitializeProjectile()
    {
        mIsExploded = false;
        mIsTriggered = false;
        mFlareParticle.Play();
        InitializeMissile();
    }
    protected sealed override void FinalizeProjectile()
    {
        Rigidbody.isKinematic = false;
        mMissileBody.SetActive(true);
        gameObject.SetActive(false);
    }

    protected virtual void FixedUpdate()
    {
        if(!mIsExploded)
        {
            Fly();
        }
    }

    protected abstract void InitializeMissile();
    protected abstract void Fly();
}
