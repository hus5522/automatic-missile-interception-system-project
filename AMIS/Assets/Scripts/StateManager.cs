﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EState
{
    NONE = -1, USER_MODE, TRAIN_MODE, TEST_MODE, COUNT
}


public static class StateManager
{
    public delegate void Action();

    //현재 상태를 변경하거나 값을 가져올때 호출하는 프로퍼티
    public static EState State
    {
        get
        {
            return mState;
        }
        set
        {
            // 현재 State와 변경된 State가 다르다면 호출
            if(mState != value)
            {
                mStateHandler[mState].Exit();
                mState = value;
                mStateHandler[mState].Enter();
            }
        }
    }

    private static EState mState = EState.NONE;
    private static Dictionary<EState, StateHandler> mStateHandler;
    
    static StateManager()
    {
        mStateHandler = new Dictionary<EState, StateHandler>();
        for(EState e = EState.NONE; e < EState.COUNT; e++)
        {
            mStateHandler.Add(e, new StateHandler());
        }
    }

    /// <summary>
    /// State 진입 이벤트 메소드 등록
    /// </summary>
    /// <param name="state"></param>
    /// <param name="action"></param>
    public static void AddEnterEvent(EState state, Action action)
    {
        mStateHandler[state].OnEnter += action;
    }
    /// <summary>
    /// State 탈출 이벤트 메소드 등록
    /// </summary>
    /// <param name="state"></param>
    /// <param name="action"></param>
    public static void AddExitEvent(EState state, Action action)
    {
        mStateHandler[state].OnExit += action;
    }
    /// <summary>
    /// State 진입 이벤트 메소드 삭제
    /// </summary>
    /// <param name="state"></param>
    /// <param name="action"></param>
    public static void RemoveEnterEvent(EState state, Action action)
    {
        mStateHandler[state].OnEnter -= action;
    }
    // State 탈출 이베느 메소드 삭제
    public static void RemoveExitEvent(EState state, Action action)
    {
        mStateHandler[state].OnExit -= action;
    }
}

public class StateHandler
{
    public event StateManager.Action OnEnter;
    public event StateManager.Action OnExit;

    public void Enter()
    {
        OnEnter?.Invoke();
    }
    public void Exit()
    {
        OnExit?.Invoke();
    }
}


