﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class FirstPersonController : MonoBehaviour
{
    public float mouseSensitivity = 2f;
    public float upDownRange = 90f;
    
    private float rotLeftRight;
    private float rotUpDown;
    private float verticalRotation = 0f;
    private float verticalVelocity = 0f;

    private CharacterController cc;

    [SerializeField]
    private GameObject upDownObject; // 상하로 움직일 오브젝트
    [SerializeField]
    private GameObject leftRightObject; // 좌우로 움직일 오브젝트
    [SerializeField]
    private GameObject cameraObject; // 터렛에 붙어있는 카메라 오브젝트
    [SerializeField]
    private Transform target; // 발사 방향을 정해줄 타겟 오브젝트
    [SerializeField]
    private GameObject missileSocket; // 발사될 소켓 위치

    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (StateManager.State == EState.USER_MODE)
        {
            RotateObject();

            if (Input.GetMouseButtonDown(0))
            {
                //발사
                ProjectileHandler.Fire(EProjectileFlags.GUIDED_MISSILE,
                    missileSocket.transform.position,
                    Vector3.zero,
                    target);
            }
        }
    }

    public void RotateObject()
    {
        rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
        leftRightObject.transform.Rotate(0f, 0f, rotLeftRight);

        verticalRotation = Input.GetAxis("Mouse Y") * mouseSensitivity;
        upDownObject.transform.Rotate(0f, -verticalRotation, 0f);
    }
}
