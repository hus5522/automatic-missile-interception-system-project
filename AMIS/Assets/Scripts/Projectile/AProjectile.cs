﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum EFireType
{
    NONE = -1, TRANSFORM, POSITION
}
public abstract class AProjectile : MonoBehaviour
{
    public Transform DstTransform { get { return mDstTransform; } set { mDstTransform = value; } }
    public Vector3 DstPosition { get { return mDstPosition; } set { mDstPosition = value; } }
    public EProjectileFlags ProjectileFlag { get { return mProjectileFlag; } }

    public TriggerHandler_Projectile TriggerHandler { get { return mTriggerHandler; } }
    public Rigidbody Rigidbody { get { return mRigidbody; } }

    public EFireType FireType { get { return mFireType; } }

    private bool mIsFired = false;
    [SerializeField]
    private EProjectileFlags mProjectileFlag;

    private EFireType mFireType;

    private TriggerHandler_Projectile mTriggerHandler;
    private Rigidbody mRigidbody;

    private Transform mDstTransform;
    private Vector3 mDstPosition;

    public void Fire(Vector3 srcPosition, Vector3 srcEulerAngles)
    {
        mIsFired = true;
        transform.position = srcPosition;
        transform.eulerAngles = srcEulerAngles;
        InitializeProjectile();
    }

    public void Fire(Vector3 srcPosition, Vector3 srcEulerAngles, Vector3 dstPosition)
    {
        mFireType = EFireType.POSITION;
        mDstPosition = dstPosition;
        Fire(srcPosition, srcEulerAngles);
    }
    public void Fire(Vector3 srcPosition, Vector3 srcEulerAngles, Transform dstTransform)
    {
        mFireType = EFireType.TRANSFORM;
        mDstTransform = dstTransform;
        Fire(srcPosition, srcEulerAngles);
    }

    protected abstract void InitializeProjectile();
    protected abstract void FinalizeProjectile();
    protected abstract void Execute();
    protected abstract bool CheckCondition();

    protected void Update()
    {
        if(mIsFired)
        {
            if(CheckCondition())
            {
                Execute();
            }
            else
            {
                mIsFired = false;
                mFireType = EFireType.NONE;
                FinalizeProjectile();
            }
        }
    }
    protected virtual void Awake()
    {
        mTriggerHandler = GetComponentInChildren<TriggerHandler_Projectile>();
        mRigidbody = GetComponent<Rigidbody>();
    }
}
