﻿using UnityEngine;
using System.Collections.Generic;

public class Trigger<ETriggerFlags> : MonoBehaviour
{
    public delegate void OnTrigger(Collider collider);

    public Collider Collider { get; private set; }
    public event OnTrigger OnEnter;
    public event OnTrigger OnExit;
    public ETriggerFlags TriggerFlag { get { return mTriggerFlag; } }

    private Collider[] mColliders;
    private List<Collider> mColliderList;
    [SerializeField]
    private ETriggerFlags mTriggerFlag;
    [SerializeField]
    private ELayerFlags[] mDetectLayers;
    [SerializeField]
    private bool mIsFixed;

    public Collider[] GetColliders()
    {
        List<Collider>.Enumerator enumerator = mColliderList.GetEnumerator();
        int index = 0;
        while (enumerator.MoveNext() && index < mColliders.Length)
        {
            mColliders[index++] = enumerator.Current;
        }
        while (index < mColliders.Length)
        {
            mColliders[index++] = null;
        }
        return mColliders;
    }

    private void Awake()
    {
        Collider = GetComponent<Collider>();
        mColliderList = new List<Collider>();
        mColliders = new Collider[20];
    }
    private void Start()
    {
        if (mIsFixed)
        {
            transform.SetParent(null);
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        int layer = collision.gameObject.layer;

        foreach (ELayerFlags detectLayer in mDetectLayers)
        {
            if (layer == (int)detectLayer)
            {
                mColliderList.Add(collision);
                OnEnter?.Invoke(collision);
                break;
            }
        }
    }
    private void OnTriggerExit(Collider collision)
    {
        int layer = collision.gameObject.layer;

        foreach (ELayerFlags detectLayer in mDetectLayers)
        {
            if (layer == (int)detectLayer)
            {
                mColliderList.Remove(collision);
                OnExit?.Invoke(collision);
                break;
            }
        }
    }
    private void OnDisable()
    {
        mColliderList.Clear();
    }
}
